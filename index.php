<?php

require 'vendor/autoload.php';

use App\Simulator;

$simulator = new Simulator();
while (true){
	$cmd = readline("Enter a command: ");	
	$out = $simulator->run($cmd);
	echo $out."\r\n";
}
