<?php 
namespace test;
use PHPUnit\Framework\TestCase;

use App\TableTop;
use App\Location;

class TableTopTest extends TestCase {

	protected $tableTop;

	protected function setUp(): void {
		parent::setUp();

		$this->tableTop = new TableTop(5,5,Location::WEST);
	}

	public function testIsValidPositionTrue() {
		$location = new Location(0,0,Location::WEST);
		$this->assertTrue($this->tableTop->isValidPosition($location));
	}

	public function testIsValidPositionFalse() {
		$location = new Location(5,6,Location::WEST);
		$this->assertFalse($this->tableTop->isValidPosition($location));
	}
	
}