<?php 
namespace test;
use PHPUnit\Framework\TestCase;

use App\Robot;
use App\TableTop;
use App\Location;

class RobotTest extends TestCase {

	protected function setUp(): void {
		parent::setUp();

		$this->tableTop = $this->createMock(TableTop::class);
		$this->tableTop
		->expects($this->any())
        ->method('isValidPosition')
        ->will($this->returnValue(true));

	}

	public function testPlace() {
		$robot = new Robot($this->tableTop,new Location(0,0,Location::EAST) );

		$new_location = new Location(0,5,Location::EAST);
		$robot->place($new_location);

		$this->assertEquals($new_location,$robot->getLocation() );
	}

	public function testMove() {
		$robot = new Robot($this->tableTop,new Location(0,0,Location::EAST) );

		$new_location = new Location(1,0,Location::EAST);
		$robot->move();

		$this->assertEquals($new_location,$robot->getLocation() );
	}

	public function testLeft() {
		$robot = new Robot($this->tableTop,new Location(0,0,Location::EAST) );

		$new_location = new Location(0,0,Location::NORTH);
		$robot->left();

		$this->assertEquals($new_location,$robot->getLocation() );
	}

	public function testRight() {
		$robot = new Robot($this->tableTop,new Location(0,0,Location::EAST) );

		$new_location = new Location(0,0,Location::SOUTH);
		$robot->right();

		$this->assertEquals($new_location,$robot->getLocation() );
	}
}