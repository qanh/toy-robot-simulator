<?php 
namespace test;
use PHPUnit\Framework\TestCase;

use App\Location;

class LocationTest extends TestCase {

	public function testIsValidDirection() {
		$testTrue = array(Location::EAST,Location::WEST,Location::NORTH,Location::SOUTH);
		$testFalse = array("Test","Robot");

		foreach ($testTrue as $test) {
			$this->assertTrue(Location::isValidDirection($test));
		}

		foreach ($testFalse as $test) {
			$this->assertFalse(Location::isValidDirection($test));
		}
	}
}