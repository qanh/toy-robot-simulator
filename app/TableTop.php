<?php 

namespace app;
use App\Location;

class TableTop {
	private $x_length;
	private $y_length;

	public function __construct($x_length, $y_length) {
		$this->x_length = $x_length;
		$this->y_length = $y_length;
	}

	public function isValidPosition(Location $location) {
		return ( $location->getXPos() >= 0 && $location->getXPos() < $this->x_length ) && ( $location->getYPos() >= 0 && $location->getYPos() < $this->y_length ) ;
	}
}

?>