<?php 

namespace app;
use Exception;
use App\TableTop;
use App\Location;

class Robot {
	protected $location;
	protected $tableTop;

	public function __construct(TableTop $tableTop,Location $location) {
		$this->tableTop = $tableTop;

		if( !$this->tableTop->isValidPosition($location) ){
			throw new Exception('Invalid position');
		}

		$this->location = $location;
	}

	public function setTableTop(TableTop $tableTop) {
		$this->tableTop = $tableTop;
	}

	public function getLocation(){
		return $this->location;
	}

	public function place(Location $location) {
		if( $this->tableTop->isValidPosition($location) ){
			$this->location = $location;
		}
		else{
			return false;
		}

		return true;
	}

	public function move() {
		$new_x = $this->location->getXPos();
		$new_y = $this->location->getYPos();

		switch ( $this->location->getFacingDirection()  ) {
			case Location::NORTH :
				$new_y = $new_y - 1;
				break;
			
			case Location::SOUTH :
				$new_y = $new_y + 1;
				break;

			case Location::EAST :
				$new_x = $new_x + 1;
				break;

			case Location::WEST :
				$new_x = $new_x - 1;
				break;
		}

		$new_location = new Location($new_x,$new_y,$this->location->getFacingDirection());

		if( $this->tableTop->isValidPosition($new_location) ){
			$this->location = $new_location;
		}
		else{
			return false;
		}

		return true;
	} 

	public function left() {
		switch ( $this->location->getFacingDirection() ) {
			case Location::NORTH :
				$this->location->setFacingDirection(Location::WEST);
				break;
			
			case Location::SOUTH :
				$this->location->setFacingDirection(Location::EAST);
				break;

			case Location::EAST :
				$this->location->setFacingDirection(Location::NORTH);
				break;

			case Location::WEST :
				$this->location->setFacingDirection(Location::SOUTH);
				break;
		}

		return true;
	}

	public function right() {
		switch ( $this->location->getFacingDirection() ) {
			case Location::NORTH :
				$this->location->setFacingDirection(Location::EAST);
				break;
			
			case Location::SOUTH :
				$this->location->setFacingDirection(Location::WEST);
				break;

			case Location::EAST :
				$this->location->setFacingDirection(Location::SOUTH);
				break;

			case Location::WEST :
				$this->location->setFacingDirection(Location::NORTH);
				break;
		}

		return true;
	}

	public function report() {
		return $this->location->getText();
	}

}

?>