<?php 

namespace app;

use App\TableTop;
use App\Robot;
use App\Location;


class Simulator {
	protected $robot;
	protected $tableTop;

	const TABLE_X_LENGTH = 5;
	const TABLE_Y_LENGTH = 5;

	const CMD_PLACE = 'PLACE';
	const CMD_MOVE = 'MOVE';
	const CMD_LEFT = 'LEFT';
	const CMD_RIGHT = 'RIGHT';
	const CMD_REPORT = 'REPORT';

	public function __construct() {
		$this->tableTop = new TableTop($this::TABLE_X_LENGTH,$this::TABLE_Y_LENGTH);
		$this->robot =  new Robot($this->tableTop, new Location(0,0,Location::EAST) );
	}

	public function run($inputString){
		$inputArr = explode(' ',trim($inputString));
		$out = "";

		switch ( strtoupper($inputArr[0]) ) {
			case $this::CMD_PLACE :
				$out = $this->place($inputArr[1]);
				break;
			
			case $this::CMD_MOVE :
				$out = $this->move();
				break;

			case $this::CMD_LEFT :
				$out = $this->left();
				break;

			case $this::CMD_RIGHT :
				$out = $this->right();
				break;

			case $this::CMD_REPORT :
				$out = $this->report();
				break;

			default:
				$out = 'Invalid Command';
				break;
		}

		return $out;
	}

	private function getResultText($success){
		$result = "";
		if($success){
			$result = "";
		}
		
		return $result;
	}

	public function place($input) {
		$inputArr = explode(',',trim($input));

		$location = new Location($input[0],$input[1],$input[2]);

		$success = $this->robot->place($location);
		return $this->getResultText($success);
	}

	public function move(){
		$success = $this->robot->move();
		return $this->getResultText($success);
	} 
	public function left() {
		$success = $this->robot->left();
		return $this->getResultText($success);
	}
	public function right(){
		$success = $this->robot->right();
		return $this->getResultText($success);
	}
	public function report(){
		$result = $this->robot->report();
		return $result;
	}
}

?>