<?php 

namespace app;
use Exception;

class Location {
	protected $x_pos;
	protected $y_pos;
	protected $facing_direction;

	const NORTH = 'NORTH';
	const SOUTH = 'SOUTH';
	const EAST = 'EAST';
	const WEST = 'WEST';

	public function __construct($x_pos, $y_pos, $facing_direction){
		$this->x_pos = $x_pos;
		$this->y_pos = $y_pos;

		if( ! Location::isValidDirection($facing_direction) ){
			throw new Exception('Invalid direction');
		}

		$this->facing_direction = $facing_direction;
	}

	public static function isValidDirection($facing_direction){
		if( in_array($facing_direction, array(Location::NORTH,Location::SOUTH,Location::EAST,Location::WEST) ) ){
			return true;
		}
		return false;
	}

	public function setPosition($x_pos, $y_pos, $facing_direction){
		$this->x_pos = $x_pos;
		$this->y_pos = $y_pos;
		$this->facing_direction = $facing_direction;
	}

	public function setFacingDirection($facing_direction){
		if( Location::isValidDirection($facing_direction) ){
			$this->facing_direction = $facing_direction;	
			return true;
		}

		return false;
	}

	public function getXPos(){
		return $this->x_pos;
	}
	public function getYPos(){
		return $this->y_pos;
	}
	public function getFacingDirection(){
		return $this->facing_direction;
	}

	public function getText(){
		return sprintf('x : %s, y : %s, direction : %s',$this->x_pos, $this->y_pos, $this->facing_direction);
	}
}